#FlashLight(PowerSaving) 

The best flashlight app to save you power and then protect you battery.

Do you encounter such a situation: When you using the flashlight to show something, the iPhone is locked. PowerSaving Flashlight also solve this issue.

Details: 

+   Saving you battery power automatic, and you can change it from settings(both in app setting and system setting) 
+   Stop using you torch when battery level is low 
+   Disable the system Auto-Lock 
+   Turn on the flashlight when app startup(You can set it to “NO” 
+   Fullscreen control, but you can only turn it off by press the button. 
+   Share the famous app to you friends.

No matter what problem you have, just contact us by email or visit the develop’s blog.

史上最懂你的省电手电筒

还在为找不到好用的手电筒发愁么？ 还在为那些“大而全”的工具启动慢、难用发愁么？ 还在为手电筒用着用着就黑了发愁么？ 还在为手电筒用着太费电发愁么？

+   自动为你的手机省电
+   当电池电量过低，自动切换到“白屏”模式
+   可选择禁用系统锁屏功能
+   启动可选择默认打开手电筒
+   全屏控制功能，按任意位置打开，关闭按开关
+   分享手电筒给好友
+   如果您遇到问题或者有好的建议，请直接与我们联系或者在程序中提交反馈，谢谢。

无奈AppStore不让上，做的再好都不让上，哎！

91：
http://app.91.com/Soft/Detail.aspx?Platform=iPhone&f_id=1577788

同步推：
http://app.tongbu.com/10001483_flashlight.html


## Todo
1.	Add CocoaPods to manage libraries.
2.	Support new XCode and enhance iOS 7 performance.