//
//  TorchConfig.h
//  Flashlight
//
//  Created by Eric on 4/14/13.
//  Copyright (c) 2013 Saick. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SAVING_AUTOMATIC    @"kSavingAutomatic"
#define STOP_USING_TORCH    @"kStopUsingTorch"

#define DISABLE_AUTO_LOCK   @"kDisableSystemScreenIdle"
#define DEFAULT_TURN_ON     @"kDefautTurnOnTorch"
#define FULLSCREENCONTROL   @"kFullscreenControl"

@interface TorchConfig : NSObject

@property (nonatomic, assign) CGFloat fStopTorch;
@property (nonatomic, assign) BOOL isSavingAutomatic;

@property (nonatomic, assign) BOOL isDisableAutoLock;
@property (nonatomic, assign) BOOL isDefaultOn;
@property (nonatomic, assign) BOOL isEnableFullScreenControl;


+ (TorchConfig *)sharedConfig;

- (void)writeCurrentToUserDefault;
- (void)updateSettingAffect;

@end
