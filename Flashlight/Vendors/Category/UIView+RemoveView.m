//
//  UIView+RemoveView.m
//  LotteryClient
//
//  Created by Eric on 8/27/12.
//  Copyright (c) 2012 Inforwave. All rights reserved.
//

#import "UIView+RemoveView.h"

@implementation UIView (RemoveView)

- (void)removeSubviews:(Class)class
{
    for (UIView *view in self.subviews) {
        if ([view isKindOfClass:class])
            [view removeFromSuperview];
    }
}

@end
