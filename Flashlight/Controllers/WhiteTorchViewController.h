//
//  WhiteTorchViewController.h
//  Flashlight
//
//  Created by Eric on 4/14/13.
//  Copyright (c) 2013 Saick. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WhiteTorchViewController : UIViewController

@property (nonatomic, retain) IBOutlet UIView *vMask;

- (IBAction)turnWhiteTorchOff:(id)sender;

@end
