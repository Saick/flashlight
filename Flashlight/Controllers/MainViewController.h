//
//  MainViewController.h
//  SmartFlashlight
//
//  Created by Eric on 11/25/12.
//  Copyright (c) 2012 Saick. All rights reserved.
//

#import "MoreViewController.h"

@interface MainViewController : UIViewController
<
MoreViewControllerDelegate,
UIScrollViewDelegate
>
{
    UILabel *_lbLCD;
}

@property (nonatomic, retain) IBOutlet UIScrollView *scroll;
@property (nonatomic, retain) IBOutlet UIImageView *background;

- (IBAction)showInfo:(id)sender;
- (IBAction)torchBtnPressed:(id)sender;

@end
