//
//  AppDelegate.m
//  SmartFlashlight
//
//  Created by Eric on 11/25/12.
//  Copyright (c) 2012 Saick. All rights reserved.
//

#import "AppDelegate.h"
#import "MainViewController.h"
#import "SQLibs.h"
#import "TorchManage.h"
#import "TorchConfig.h"
#import "global_Header.h"

@implementation AppDelegate

- (void)test
{
    
}

- (void)singletonDealloc
{
    [[TorchConfig sharedConfig] release];
    [[TorchManage defaultManager] release];
}

- (void)dealloc
{
    [_window release];
    [_mainViewController release];
    
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // 默认AppStore
    NSString *strChannelPath = [[NSBundle mainBundle] pathForResource:@"UMChannelID" ofType:nil];
    NSError *error = nil;
    NSString *strChannelID = [NSString stringWithContentsOfFile:strChannelPath encoding:NSUTF8StringEncoding error:&error];
    [MobClick startWithAppkey:kMobClickAppKey reportPolicy:BATCH channelId:strChannelID];
    
    application.statusBarHidden = NO;
    
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    // Override point for customization after application launch.
    self.mainViewController = [[[MainViewController alloc] initWithNibName:@"MainViewController" bundle:nil] autorelease];
    self.window.rootViewController = self.mainViewController;
    [self.window makeKeyAndVisible];
    
    // default on
    if ([TorchConfig sharedConfig].isDefaultOn == YES)
        [[TorchManage defaultManager] showTorch:1.0f];
    
    [self test];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
    [MobClick event:@"applicationWillResignActive"];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [MobClick event:@"applicationDidBecomeActive"];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    [self singletonDealloc];
    [MobClick event:@"applicationWillTerminate"];
}

#pragma mark - check install infor

// 如果取到数据说明是已经安装过，第一次启动后调用些方法（暂未使用）
- (void)install
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    if (![userDefault boolForKey:kFirstInstall])
        [userDefault setBool:YES forKey:kFirstInstall];
}

@end
